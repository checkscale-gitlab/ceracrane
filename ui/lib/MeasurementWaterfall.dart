import 'dart:async';
import 'dart:collection';
import 'dart:core';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:eventify/eventify.dart' as Eventify;
import 'package:sample/StyleUtils.dart';
import 'Constants.dart' as Constants;
import 'StyleUtils.dart';
import 'WaterfallLine.dart';

class Waterfall extends StatefulWidget {
  final Eventify.EventEmitter emitter;
  const Waterfall(this.emitter);
  @override
  _Waterfall createState() => _Waterfall();
}

class _Waterfall extends State<Waterfall> {
  Timer blockingTimer;
  Queue<int> statusQueue = Queue<int>();
  void tick() {
    if (blockingTimer == null || !blockingTimer.isActive) {
      blockingTimer = new Timer(
          new Duration(
              milliseconds: 500 + (4000 * Random().nextDouble()).toInt()), () {
        int newJobStatus = statusQueue.removeFirst();
        setState(() {
          _jobStatus = newJobStatus;
        });
        if (newJobStatus == 5) {
          Timer(Duration(seconds: 1), () {
            widget.emitter.emit("waterfalldone");
            statusQueue.clear();
          });
        }
        if (statusQueue.length > 0) {
          tick();
        }
      });
    }
  }

  void init() {
    setState(() {
      _jobStatus = 0;
      _target = 0;
    });
  }

  void failed() {
    blockingTimer.cancel();
    setState(() {
      _jobStatus = -1;
      _target = -1;
    });
  }

  void blockUpdates() {}
  bool blocking = true;
  int _jobStatus = -2;
  int _target = -2;
  bool updateThisCycle = false;
  void initState() {
    super.initState();
    widget.emitter.on("reset", this, (ev, cont) {
      failed();
      widget.emitter.emit("waterfalldone");
    });
    widget.emitter.on("job", this, (ev, cont) {
      if (ev.eventData as int == 0) {
        init();
      } else if (ev.eventData as int > _target) {
        for (int i = _target + 1; i <= ev.eventData; i++) {
          statusQueue.addLast(i);
        }
        setState(() {
          _target = ev.eventData;
        });
        tick();
      } 
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      AnimatedOpacity(
          duration: Duration(milliseconds: 200),
          opacity: (_jobStatus == -2 ? 1 : 0),
          child: (Text(Constants.JOB_STATUS[_jobStatus],
              style: bodyBold(context)))),
      AnimatedOpacity(
          duration: Duration(milliseconds: 200),
          opacity: (_jobStatus == -1 ? 1 : 0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("ERROR", style: redBodyBold(context)),
                Text(Constants.JOB_STATUS[-1], style: body(context))
              ])),
      AnimatedOpacity(
          duration: Duration(milliseconds: 200),
          opacity: (_jobStatus >= 0 ? 1 : 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              WaterfallLine(id: 0, jobStatus: _jobStatus),
              WaterfallLine(id: 1, jobStatus: _jobStatus),
              WaterfallLine(id: 2, jobStatus: _jobStatus),
              WaterfallLine(id: 3, jobStatus: _jobStatus),
              WaterfallLine(id: 4, jobStatus: _jobStatus),
              WaterfallLine(id: 5, jobStatus: _jobStatus),
            ],
          ))
    ]);
  }
}
