import 'dart:convert';
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'Constants.dart' as Constants;
import './searchLib/SearchBar.dart' as SearchBar;

class Measurement {
  final String craneName;
  final double loadGross;
  final double loadTared;
  final DateTime dateTime;
  final String containerId;
  final String fingerprint;
  final String xmlString;
  final String dccString;
  final String iotaHash;
  final bool iotaConfirm;
  bool weightTampered;

  Measurement(this.craneName, this.loadGross, this.loadTared, this.dateTime, this.containerId, this.fingerprint, this.xmlString, this.dccString, this.iotaHash, this.iotaConfirm);

  Measurement.fromJson(Map<String, dynamic> json)
    : craneName = json['crane_name'],
      loadGross = json['load_gross_t'],
      loadTared = json['load_tared_t'],
      dateTime = DateTime.parse(json['date_time']),
      containerId = json['container_id'],
      fingerprint = json['fingerprint'],
      xmlString = json['xml_value'], 
      dccString = json['dcc'],
      iotaHash = json['iota_tx_hash'],
      iotaConfirm = json['iota_confirmed'];
}

//List must be in date order
void checkWeights(List<Measurement> measurements, double weightDelta) {
  if (measurements.isEmpty) return;
  measurements.first.weightTampered = false;
  if (measurements.length == 1) return;
  double minWeight = measurements.first.loadTared - weightDelta;
  double maxWeight = measurements.first.loadTared + weightDelta;
  for (int i = 1; i < measurements.length; i++){
    if (measurements[i].loadTared < minWeight || measurements[i].loadTared > maxWeight){
      measurements[i].weightTampered = true;
    }
    else {
      measurements[i].weightTampered = false;
    }
  }
  return;
}

class SearchWrapper extends StatefulWidget {
  @override
  _SearchWrapperState createState() => _SearchWrapperState();
}

class _SearchWrapperState extends State<SearchWrapper> {
  final SearchBar.SearchBarController<Measurement> _searchBarController = SearchBar.SearchBarController();
  bool isReplay = false;

  Future<List<Measurement>> _getALlPosts(String text) async {
    await Future.delayed(Duration(seconds: text.length == 4 ? 10 : 1));
    http.Response resp;
    try {
        print("Search from API");
        resp = await http.get(
            Constants.dbApiAddress + '/api/read?containerid=' + text); 
        print(resp.body);
      } catch (err) {
        print(err);
      }
    if(resp.statusCode != 200) throw Error();
    
    List<dynamic> list = json.decode(resp.body);
    List<Measurement> measurements = [];
    for (int i = 0; i < list.length; i++) {
      Map<String, dynamic> jsonObject = list[i];
      Measurement single = new Measurement.fromJson(jsonObject);
      measurements.add(single);
    }
    measurements.sort((a, b) => a.dateTime.compareTo(b.dateTime));
    checkWeights(measurements, Constants.maximumAcceptedWeightDelta);
    return measurements;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: SearchBar.SearchBar<Measurement>(
              searchBarPadding: EdgeInsets.fromLTRB(20, 20, 10, 10),
              headerPadding: EdgeInsets.symmetric(horizontal: 10),
              listPadding: EdgeInsets.symmetric(horizontal: 10),
              onSearch: _getALlPosts,
              hintText: "Search with container-id",
              hintStyle: TextStyle(
                color: Colors.grey[700],
                fontWeight: FontWeight.w300,
              ),
              searchBarController: _searchBarController,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              crossAxisCount: 1,
              onItemFound: (Measurement measurement, int index) {
                return Container(
                  child: Card(
                    elevation: 3,
                    child: ExpansionTile(
                      title: Text((index + 1).toString() + ". " + measurement.craneName),
                      subtitle: Text(
                        DateFormat("kk:mm:ss").format(measurement.dateTime) +
                        " UTC+0, " + 
                        DateFormat("EEE d MMM").format(measurement.dateTime)
                      ),
                      trailing: Wrap(
                        spacing: 12,
                        children: <Widget> [
                          Tooltip (
                            message: measurement.weightTampered ? "Weight does not match original measurement" 
                            : !measurement.iotaConfirm ? "Measurement not confirmed by blockchain"
                            : "Measurement is valid",
                            child: Icon(
                              measurement.weightTampered ? Icons.cancel 
                              : !measurement.iotaConfirm ? Icons.cancel
                              : Icons.check_circle,
                              color: measurement.weightTampered ? Color(0xffe15554) 
                              : !measurement.iotaConfirm ? Color(0xffe15554)
                              : Color(0xff3bb273),
                            ),
                          ),
                          Icon(Icons.keyboard_arrow_down),
                        ]
                      ),
                      children: <Widget>[
                        new ExpansionCardContent(measurement: measurement)
                      ],
                    ),
                  )
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class ExpansionCardContent extends StatelessWidget {
  final Measurement measurement;
  ExpansionCardContent({this.measurement});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      minimum: const EdgeInsets.all(16.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Text("Weight: " + measurement.loadGross.toStringAsFixed(3) + " Tkg"),
            Text("Tared weight: " + measurement.loadTared.toStringAsFixed(3) + " Tkg"),
            ListTile(
              title: Text("Show measurement XML"),
              onTap: () async {
                String url = Constants.dbApiAddress + "/api/xml?tx=" + measurement.iotaHash;

                if (await canLaunch(url)) {
                  await launch(url, forceSafariVC: false);
                } else {
                  throw 'Could not launch XML';
                }
              },
            ),
            ListTile(
              title: Text("Show measurement DCC"),
              onTap: () async {
                String url = window.location.protocol + "//" + window.location.hostname + ":10001/XML/" + measurement.dccString;
                print(url);
                if (await canLaunch(url)) {
                  await launch(url, forceSafariVC: false);
                } else {
                  throw 'Could not launch DCC';
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
