import 'package:flutter/material.dart';
import 'package:sample/LandingPage.dart';
import 'package:sample/OperatorView.dart';
import 'package:sample/App.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ceracrane',
      home: App(),
      routes: {
        '/LandingPage': (BuildContext context) => LandingPage(),
        '/OperatorView': (BuildContext context) => OperatorView(),
      },
    );
  }
}
