# eIDAS Document signing
These services are responsible for digitally signing the measurement XML's. The [REST service](https://gitlab.com/kurkimiehet/ceracrane/-/tree/deploy/eidas-dss/REST) is in this project responsible for validating the signed XML files and the [XAdES signer](https://gitlab.com/kurkimiehet/ceracrane/-/tree/deploy/eidas-dss/XaDesSigner) is responsible on creating the XML files with signatures.

## This approach is not recommended!

To create XAdES signed documents please refer to [this project](https://github.com/AaltoSmartCom/dss-demonstrations). It contains [documentation](https://github.com/AaltoSmartCom/dss-demonstrations/tree/master/documentation) on how to create the signatures. 

**It is unnecessary to divide the signing and validating to two different services.**

## What is eIDAS DSS?
***eIDAS** (Electronic Identification, Authentication and Trust Services) is an EU regulation on electronic identification and trust services for electronic transactions in the European Single Market.* ([Wikipedia](https://en.wikipedia.org/wiki/EIDAS))

**DSS** is a [Digital Signature Service project](https://github.com/esig/dss) which can be used to create eIDAS signatures. More information on the eSignature EU project can be found at the [CEF website](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eSignature).

Demonstration of the running DSS WebApp can be found [here](https://ec.europa.eu/cefdigital/DSS/webapp-demo).

Documentation regarding the DSS project can be found [here](https://ec.europa.eu/cefdigital/DSS/webapp-demo/doc/dss-documentation.html). It is a very detailed documentation on what can be done with the DSS software. 