# OPC-UA Crane access
These services are responsible of retrieving the data from the [Ilmatar crane](https://www.aalto.fi/en/industrial-internet-campus/ilmatar-open-innovation-environment). There are three different services:

 - [Rest API](https://gitlab.com/kurkimiehet/ceracrane/-/blob/master/opcua/src/RestAPI.py)
 - [Mockup crane](https://gitlab.com/kurkimiehet/ceracrane/-/blob/master/opcua/src/server.py)
 - [Crane interface tests](https://gitlab.com/kurkimiehet/ceracrane/-/blob/master/opcua/src/clientTests.py)

## What is OPC-UA?
**OPC Unified Architecture** (**OPC UA**) is a machine to machine communication protocol for industrial automation developed by the OPC Foundation. ([Wikipedia](https://en.wikipedia.org/wiki/OPC_Unified_Architecture))

This means that by modifying the variable names these services are compatible with any measurement tool which uses OPC-UA protocol to communicate.

# Rest API
The rest API is used to automate the process of retrieving the data from the crane, signing the measurement and then sending the measurement to the database and Iota API.
## Running the API
The Docker image can be built manually from [this Dockerfile](https://gitlab.com/kurkimiehet/ceracrane/-/blob/master/opcua/Dockerfile_restapi). It is however recommended to use [docker-compose](https://gitlab.com/kurkimiehet/ceracrane/-/blob/master/docker-compose.yml) as you need to have the following services running:
* eIDAS REST
* XAdES Signer
* opcua mockup server or connection to crane

The API takes the following environmental variables to configure the addresses of the different services:

 - ```crane_address```
 - ```xades_address```
 - ```eidas_address```
 - ```dbrest_address```

It is again recommended to use docker-compose as the API defaults on docker-compose internal connections. This means that if you are using docker-compose you do not need to specify any of these variables.

When using docker-compose the service starts up at:
```http://127.0.0.1:9000/```

## Endpoints
 All of the returns are in JSON format. In error situations the API returns the following form:
 

    {'message' : 'Error message'}
<br/>

**Getting current weight from the crane:**
```GET at /cranestatus```

Arguments: None

Returns :

    {
	    'weight' : Crane load as double
	    'tared'  : Tared load as double
	    'datetime' : Current datetime from the crane
    }

<br/>
<br/>

**Creating a new measurement job:**
```POST at /createjob```

Arguments:
```id``` - The id of the measured container

Returns:

    {
	 'status': "Job created",
	 'statusnum': 0,
	 'jobid' : UUID for this specific job,
	 'data': {},
	 'xml': ""
	 }

<br/>
<br/>

**Getting the status of a specific measurement job:**
```GET at /status```

Arguments:
```id``` - The UUID of the specific job

Returns:

    {
	 'status': "Job created",
	 'statusnum': 0,
	 'jobid' : UUID for this specific job,
	 'data': {},
	 'xml': ""
	}
Explanation of fields:
```status``` - String representation of the status
```statusnum``` - Numerical representation of status ( 0 - 5 , -1 if job failed)
```data``` - The measurement data will be available here
```xml``` - The measurement xml will be available here as string

# Mockup crane

Our stack currently includes a *mockup crane* that performs identically to the real crane we were developing againts.
This way we could develop our product safely without having a connection open 24/7 to the AIIC Ilmatar Crane.

The OPC-UA server schema is defined by custom XML and can be viewed [here](./src/xml/custom_nodes.xml).
The server code can be viewed [here](./src/server.py)

# Tests

Test are implemented againt's our mockup crane to minimize 3rd party depedencies. They can be viewed [here](./src/clientTests.py)
