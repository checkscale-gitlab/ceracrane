xmlTemplate = """
<?xml version="1.0" ?>

<ev:measurementEvent xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://NamespaceTest.com/measurementEvent"
  xmlns:si="https://ptb.de/si"
  xmlns:ev="http://NamespaceTest.com/measurementEvent"
  xmlns:m="http://NamespaceTest.com/measurementMetadata">

<ev:metadata>
  <m:craneName>%(crane_name)s</m:craneName>
  <m:dateTime>%(datetime)s</m:dateTime>
  <m:containerId>%(containerid)s</m:containerId>
</ev:metadata>

<ev:measurement>
  <si:real>
    <si:label>load_tared</si:label>   
    <si:value>%(load_tared)s</si:value>
    <si:unit>\kilogram</si:unit>
    <si:expandedUnc>
      <si:uncertainty>0.50</si:uncertainty>
      <si:coverageFactor>2</si:coverageFactor>
      <si:coverageProbability>0.95</si:coverageProbability>
      <si:distribution>normal</si:distribution>
    </si:expandedUnc>
  </si:real>

  <si:real>
    <si:label>load_gross</si:label>   
    <si:value>%(load_gross)s</si:value>
    <si:unit>\kilogram</si:unit>
    <si:expandedUnc>
      <si:uncertainty>0.50</si:uncertainty>
      <si:coverageFactor>2</si:coverageFactor>
      <si:coverageProbability>0.95</si:coverageProbability>
      <si:distribution>normal</si:distribution>
    </si:expandedUnc>
  </si:real>

</ev:measurement>
</ev:measurementEvent>  
    """

signTemplate = """{
  \"parameters\" : {
  },
  \"keystore\": {
  	\"key\": \"%(keystorePassword)s\",
  	\"bytes\": \"%(base64Keystore)s\"
  },
  \"toSignDocument\" : {
    \"bytes\" : \"%(base64Document)s\",
    \"digestAlgorithm\" : null,
    \"name\" : \"RemoteDocument.xml\"
  }
}"""

mockupKeystore = "MIIQKQIBAzCCD+8GCSqGSIb3DQEHAaCCD+AEgg/cMIIP2DCCBg8GCSqGSIb3DQEHBqCCBgAwggX8AgEAMIIF9QYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQI5BqWs67cM84CAggAgIIFyHMLKXE7gdYvznJo9S8rGx9rmLUrsb9wUbn8k4YTUc3E00SdCawyOO0z6GYO+l0nvzAcXGMaK6z7IDE3QUiKnL8WNliB6hjs2iKX7Hl6c725lFwhSICRKarnBHpKWKwydpuD6PEe59jKgVY0okX1QycYDRkcTVGIiUTRJ34ugiyoi//VYMef1aZba7MLCN8XBhpJKpIVHNcmTIlurAEjNfSxQjeAKyL9mZ9ZTqjVihuP1eUl+QrS3ysMu+pQ9L61da1Ftt/cK3cYPrDJlAeRH8CT7aAPyPqeOi5Gq3Mvu81PbYZp0apRvH/1d5vTDTYt2/yVnBGkodzO3Jhpp6vCqDCb2nU0S0N0EmgKY1+upXltfwSpRvmJFIZUgBp8dC5ZOnRE7kU7GT+MzuOw8QM+hKZA4AcZIiVehB+oTjtKPrIOOy69bWl4eucO3ncDpyfaKjf298ovWxF1ar8X7yPAQt0+f7nReQpyMnzvGdPwDXkBPoNSbq4uneQWXVIrHojam5UjMMGGM3WrxIrC0ZA14WLTleg4B6bvU+a2CLV34QKc1nAvPeEgbtpFtMpJZKXCwrnQprgdSv7XXpUB5AsAcjdgFkU5utLIk1+aMzOVHqsnfLECHCpE3vEwluEvffk1/zIGSSAVscyP/AOlW+IIEnitgiHs14SbZvSdyupyJsjkaQhNB9YKZaOufaThKIED+xndPGcjD9RQ/PJq5CdNblNaAoTqaDJ2LCg/N7aj+u6MGLOqDC2q6JXN2meSRaaCdZVgmT1snJE41QNIJcGxHJLwNi4rIdu+y4AClFuZ2hHwwHT15ddeOL+5v0fQfGAvHxqlalfOkzG65kuLFQuKQsytnVpgXX+XV+1EM6rLZQyUkXn005J0QfsXOfQFg3nS7pKonB25YVRCkT7b8mcR64GxpZkSw+lanWWU8LDBSVyQPVupKhy6o6l8SGtEaSJ115L7BIb4JKtC0EHPRHT3G21x092Oxv3Axix6xsNsKNTDshXcPi6yHZ8E3iz15Edc8vF5ccBXQcwlm57raVk29T/Ya/lNPBxR1CPUKEUIF1bMfNKLWql4WyDCQ5txn2xU5Oha6/z5cOSEpxJLxQCCZ6LldJ61qDStIGh1rbxdBMSVDwNphDx5mm58SIO/IYUUac0qtWsqCI1Kxj/wGCoRoD1nG4ITgvkcrpd3veof3ZslEmChYqbZ9NHtibl7GEv5rR1Ny/dLiok12Vx18phhHic40qDMKRnzvqvwuQDsyR6DfWqwCuDxrbNmlut/WgDbCTVPmBzkslbKD0zASoOHL0Z0GBhI7QAeVxZJrhrO7hWODaGcaZtSJnkDcEzIf+Z8K/e8kl1ygPPjelBNBJ7Yqg+yeEv7GvmObMJZCTXdXuwjCDQyRcOxPRz3QWb0Ue2iRcb0XtGX2RgidkBHtSs7FJc1r7VGd+umppZRl2fL6WN2CRdvdyI5HDMMXbmvwkH/F5d9RPgq9XUapVZ2Sfn+64K2HVrG7UQvLZWCgX6gqXF63FArMkgHKBvx6TmG36pO7gOinFDqDajS53Un/JvHITmehwdD92+Fg2AF9Pmzllpt0X6Z4JrwojeW05PxIwWuLYrh4qp/4tHVsSLs06WY3QM4CiC18JaXPTMFKoS1195w8FhUc46v8YXddC+T6iuvTSeWmKPzpjGlPHUehQJSQhRJwiOEsnbTN87ZTcR/5xxcpJrRRHs20prDpQhTSm6r3DNvaotyFV2Cyti7vWucBDXZg/gg00o/czfMfPRkAfFwkJhj3lOlT/mcxDTAVuT0USp8CX31A3FFZhkg1shyi2wn2WbVvf/ZhOKlujOTtobZpe9/fFg1f28BVm5pXyjvjUy0LWEEiLN0v8AJ8bVvhv8aiCCqZk8+2b+1KNflT0fNUyQ7w7x/4ISA+b8C7UWohb0pplNzvCddnMPqt43f/1sPVMn4/nfDNUKbJVwodv8TqQTjOg34SPMwggnBBgkqhkiG9w0BBwGgggmyBIIJrjCCCaowggmmBgsqhkiG9w0BDAoBAqCCCW4wgglqMBwGCiqGSIb3DQEMAQMwDgQIUo830mxAdt4CAggABIIJSBKheSvXp9YIHsU9YsT1/XBWlr6FkiiatIYqxbq541aBVkXdHbwYDdD8sG9wFXDbra9pr6w7ZmtNMGRyVf+xsoo+gAKJXbNx1pe54f5QB1tg8b/ZX8UQc1pY2GR5A4cjI2QjFNSAso0uzreIiwpGqYMTxnRnp1GSOCc27wJX8y74z6snBaKKVkY/AtS3FeD2CRXWLTG9AlqcB4ie9P2WniyVmX+JqkCta+xwJBHknQgpIaSXHubAV5HzBQ8RlcTy2WrvDVv2K1AieLuENYDYQDSiE/pxFCFYAinZ7i4mFBTGK9rL8oILnVxOri+75CQ7hzDuDnM6pXQCBbbAnyxVsLedpQXx4lj2Q+cT8JUpgXbWnTZ0bFPq41wBUiqm9l6IH5Ay3ZLlGYqiK2n+doMZh5RA/LlIzoj0y7VzVvnaky556NI5XKdGsfetDm7H7BpXMpJtCMJz1HE0utlXU6Pdw3PlGK+cjINQ9+lXVvGB2gCuKUx6O1gtwDq7CdP9akq+kqEL8ILWFAT3FcP6xaZfYmN7rLpSmCg6AWdOA/qtyR5p43AukBxpyZVuTQGoGus2M/Ogefh/eK+BxYvF+t6GLkHwcMC3gDKSpCINxrLKt7Ku3Bui824u7d4nNzmz5xX+Lo9EGJTaUdjuxQbD9x5YaNIuFxfRaC2DbFnTRL87w2/JERIt4MJvxot2LHby47cdajWbLWMukdx/GS/LkXfCnk0eyWQ1UDu5EfHHPDeugunOCMLRXloZM7REy9YK6IxBgdYFQSMLDJIsJIZeISAeoR42uZbwcKIVmeH3iivXpXV8YtI14u9VxdJFUtwkrVVN7+lnfSBE9vha5R6YLLlmo7GBq1IEQPz27lMVvSOfzB5EiE4fCWJgBPgkUzkhM8bz21wd5bdf86owuZpf4ePf1RXSt8jMey3d2ze60cdgbLTPSekJz6/sw3bjgUqtBRsJoQnRoSQ832RfvYG4SX1TuMm8yQnqEfYo4shjrlxttMxuDUXksOG0S/h0yRW1JasuiMxZfy0ZkLlEqgPOdI+p0fjed/LpMYAAxDeVKkBoVJtmsatTIttZMBVrbUt28EXei8QxZ6vcplr+BIKQzxSxCvwe5HcRHtA7ee5JV3Vup2fsCSbzhKZ0fMvr5JIHZogRtBsUXUakL0+Tv2aoek/1YzXnI2QOYy8lVJxSQ3yDQmG7NfGuzyzjJ6+Rp305KXvideeoY7g/xNIluRqwZt/jmzdi7FwCXDpC/mKJmpN5ndcLSGlAU9Gr52rf5QfZOVJzsmRyD3sOUGlbZ2twIlpLFsqXgsmzjXvUYKTBnt8z7kd8hV/BXikAMnJyaB+zn1oJOgdMMJzTfyF7DSlY+iSexm6m3mYx97TpjSlm3lABk2tPbg+b2IEDdIPCGZY91G34JakgyFkEE7knfROL8Q5WNBYymVoynAtHTQ5hI5g8mBQlP2v3CxnnoFHG0ILrWlnNSYa3nqU7wuI5+Sd0MEpwSbdwQywfSUpZrJ+Rtofngbn6D7+a2GY8035TU4CS4XMzJhz3zqYXJ0a3Q7IQ/E47Rk9dpdCyTbdax99woFD0pmPG61Zs90PzYAiepUuoFWqjHkC0hwJG0hzsSEdAwXSAMTSiqKTwIccu31vgiPXzbJEpEi9kRN8ul4HmViNv+2Y8DiWuIxfJ3ybxqNCE/v/oWI0XmTR3OSCtiCN8T27xaRypnOmtXMhZOKY2i6Wt4c0ubjrRI5PnftIgM50j53xcMhT2sUCNM+t9Fh8u3+1RviakgkGpEuDgevHvVWgU9ci3cUBFjrDtju5Eo4PJFBMqUdNi0KMfaMn7t7an1qCYhlN8F1vd2gRj7bhTiVDmsZ+TbWfdwLBaEmePLLR7AO/fnUGS8AXe3Sxgvdcdd1+oKn82VmE4FbUAaNlLXxkaB/ow0Rf8q+Uv/prISoCxtX25L5m29rssOQMD8aqoI5oa20mWPVTCEDY/TqwCg0xMcp/Z4RbFeLF8d/aeafedDwj657XiLpJP6ptU0o0lXn65CpPniVbr7yDukPsP6gx5Ut926zAdvcZlE3miEdcVK/giHj3a14Jsr6YstvVZVHC1IX+0F6htPKoawyIsBqCz/rFbfKz+sJcXebOFOSt/sHOJrbx+PQwaBzCQbKl5788MQ/DwJxHYz0hz7YANA241LUH0FbKCuK7WMp2T/B5Ttve+idqw0NdIG9FJ1hIcRv84I7A7e/a93oWOA+IGm8wg+7lVKGjIDiHGbyb/Hq50pN4lcDGNGsejwVkPtR4+EhES5DCdED2tSmTBcoWcgMCIWxvIKtf3At9ORLnQ9JaHmQRdZwNfuFJ5u/4rvexlhQJSs/Jv1A7RtQ0cyV+LtK8xpJpKV+K40ORvaiejGGm4LP3cjAvZU5hYUoO2CC+ivNU2CI+yEr6GmblZVmxG3D60R0SKM/vAx0l95G5KfR0BES5WmVWaNS5FXFYHE6DQZ3cm71tnTJaADk11AXRn01KZ5f0xKm8kDhV9AyCEUdt8RRjDU1wEn9Fq61PEs8ADi3JtQOP932bdVZho3735NytDjTVQagyXQc7ze3yTTi/aocumvf7eC6aa6tYVv2QLMshdplPgE1uv7uUfizbYxqvufYVEBDtdG1Cc8Q06QCsxKA4lzUS81rM/7BbZ/0qAJz8EaJ/FnvWUh6MAgZxAzw5mW/nDbPutAu0rC0PEgmXu+VnK8nvixsxkKhnNx8HeJqK2KE3XXn6Zp7avKgKO7f93+dxfK1lCZi8Q8MmRbpxXOcz6Xqc80iChlnZw5ndoidzMppr9sw9aoe8DPxIWUI3u3PMJkxf+9+82dxgIv1XF+R0rYVgZPEJvyvUJPK/Ne11qnRFR1jdHB+DLSzbSzUcLj04FSr546JNnKXqmiiq8EsfDHxZU9kxO0+svdeHaruLBfXQEeOwcG2lBpJWeCWUnwCnapUPWjcOIpYvTUl90DNixkoXnf2ffTL2mtUuotLthcou7NTh/7IzC7gzp2P7ffofSka52FpHJZ5uak6Lyu9U9jlc+FkwNCrgYR1WXcFBbLcU2h7zBx6fc1n4fA+nRGoGqqr1lpvwd5HetGim3M1dlq2EUVQzSx/KD9AHRWOHDuuBUuiRMm/2hVSNRRTwziEAePO8HjDgvmm7TIEvN7FbeJQ13G7tQ/7MoEzElMCMGCSqGSIb3DQEJFTEWBBRGRhgfHraX4DjyAAa3NsbFR74CgDAxMCEwCQYFKw4DAhoFAAQUZkT8ruL82zqtzqIm3PpAny2MEywECLgYZ7igV/MQAgIIAA=="
mockupKeystorePassword = "ebin"

validationTemplate = """{\r
  \"signedDocument\" : {\r
    \"bytes\" : \"%(base64signed)s\",\r
    \"digestAlgorithm\" : null,\r
    \"name\" : \"%(SignedName)s\"\r
  },\r
  \"originalDocuments\" : [ {\r
    \"bytes\" : \"%(base64original)s\",\r
    \"digestAlgorithm\" : null,\r
    \"name\" : \"%(originalName)s\"\r
  } ],\r
  \"policy\" : null,\r
  \"signatureId\" : null\r
}"""

simpleValidationTemplate = """{\r
  \"signedDocument\" : {\r
    \"bytes\" : \"%(base64signed)s\",\r
    \"digestAlgorithm\" : null,\r
    \"name\" : \"%(SignedName)s\"\r
  },\r
  \"policy\" : null,\r
  \"signatureId\" : null\r
}"""
