import requests
import base64
import json
from templates import validationTemplate, simpleValidationTemplate

"""
Verifies the integrity and signature of signedXML againts originalXML
@return: validation report in JSON
"""
def validate(originalXML, signedXML, url="http://localhost:8080/services/rest/validation/validateSignature"):
    base64_original = str(base64.b64encode(originalXML.encode("utf-8")),encoding='utf-8').strip()
    base64_signed = str(base64.b64encode(signedXML.encode("utf-8")),encoding='utf-8').strip()

    data = {
        'base64original' : base64_original,
        'originalName' : 'RemoteDocument.xml',
        'base64signed' : base64_signed,
        'SignedName' : "RemoteDocument-signed-xades-baseline-b.xml"
    }

    payload = (validationTemplate % data).encode('utf8')
    headers = {
    'Content-Type': 'application/json; charset=utf-8'
    }
    response = requests.request("POST", url, headers=headers, data = payload)
    response_data = json.loads(response.text.encode('utf8'))
    return response_data

def simpleValidate(signedXML, url):
    base64_signed = str(base64.b64encode(signedXML.encode("utf-8")),encoding='utf-8').strip()

    data = {
        'base64signed' : base64_signed,
        'SignedName' : "RemoteDocument-signed-xades-baseline-b.xml"
    }

    payload = (simpleValidationTemplate % data).encode('utf8')
    headers = {
    'Content-Type': 'application/json; charset=utf-8'
    }
    response = requests.request("POST", url, headers=headers, data = payload)
    response_data = json.loads(response.text.encode('utf8'))
    return response_data

def signatureIsValid(signedXML, url="http://localhost:8080/services/rest/validation/validateSignature"):
    response = simpleValidate(signedXML, url)
    signatureReport = response["DiagnosticData"]["Signature"][0]
    #Structural validation
    structure = signatureReport["StructuralValidation"]["Valid"]
    #Signature validation
    signature = signatureReport["BasicSignature"]["SignatureIntact"] and signatureReport["BasicSignature"]["SignatureValid"]

    return structure and signature