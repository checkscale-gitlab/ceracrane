#!/bin/bash
trap ctrl_c INT
function ctrl_c() {
  exit 0
}
LTIME=`stat -c %Z .`
schema="measurementEvent.xsd"
file="measurementEvent.xml"
clear
xmllint --schema $schema $file --noout
while true    
do
  ATIME=`stat -c %Z .`
  if [[ "$ATIME" != "$LTIME" ]]
  then    
    LTIME=$ATIME
    clear
    xmllint --schema $schema $file --noout
  fi
  sleep 0.1
done
