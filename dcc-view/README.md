## DCC-XML-CSS-UI Viewer
This is a demo on viewing measurements' associated digital calibration certificates visually.
Original repository can be found [here](https://github.com/AaltoSmartCom/DCC-XML-CSS-UI-demo).

## Usage
**Running the demonstration with Docker:**

    docker build -t dcc . && docker run -it -p 10001:10001 dcc
<br/>

You can now visually view the DCC's at

```http://127.0.0.1:10001/XML/<DCC filename>```

The filenames can be found [here](https://github.com/AaltoSmartCom/DCC-XML-CSS-UI-demo/tree/master/Project%20files/XML).