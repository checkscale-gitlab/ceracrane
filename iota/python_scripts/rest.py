from flask import Flask
from flask import jsonify 
from flask import abort 
from flask import request
from flask import json
from flask import Response
from iota import Iota
from iota_interface import confirmed_write
from iota_interface import read_from_tangle
from iota_interface import txs_as_dict
from iota_interface import get_decoded_tx_data
import iotaconf
from db import CeraDB
from psycopg2 import sql
import json
from flask_cors import CORS, cross_origin
import uuid
from threading import Thread
import hashlib
import hmac
from cryptography.fernet import Fernet
from string import ascii_letters
from string import digits
from string import punctuation
from time import sleep


# Constants used in db-rest: 
#     - tag prepended to the measurement hash and its length
#     - key for the Fernet encryption suite
CERATAG = 'CERA'
CERATAGLENGTH = len(CERATAG)
FERNETKEY = iotaconf.FERNET_KEY
CID_CHARS = ascii_letters + digits + punctuation

fernet_suite = Fernet(FERNETKEY)


def to_string(t):
    """ Convert format not recognized by json.dumps to a string."""
    return t.__str__()

def tuple_to_dict(tuplelist, colnames):
    """Convert tuples to dicts with the given column names."""
    assert len(tuplelist) == len(colnames), \
            "Number of column names and columns in the query do not match."
    return dict(zip(colnames, tuplelist))

def validate_container_id(cid):
    return len(cid) > 0 and all([ c in CID_CHARS for c in cid ])
    

def measurement_hash(measurement):
    """
    Calculate sha256 hash from a dict of measurement data and prepend
    the hash by the tag used for validating data stored in Iota.

    :param dict measurement: Dict representing measurement data with db columns
                             names as keys and their respective values as 
                             dict values. 
    :return: Iota validation tag appended with JSON of a tuple containing some
             fields from measurement data.
    :rtype: str
    """
    # The order of the fields is fixed with tuple.
    measurement_tuple = (
            measurement['crane_name'],
            # Rounding prevents float accuracy errors between db and Python.
            round(float(measurement['load_gross_t']), 6),
            round(float(measurement['load_tared_t']), 6),
            measurement['date_time'],
            measurement['container_id'],
            measurement['fingerprint'],
            measurement['xml_string'],
            measurement['dcc']
            )
    bstring = json.dumps(measurement_tuple, default=to_string).encode('utf-8')
    m = hashlib.sha256()
    m.update(bstring)
    return CERATAG + m.hexdigest()

def check_integrity(measurement, iota_hashes):
    """Check that the measurement in db is not altered against Iota."""
    m_hash = measurement_hash(measurement)
    found = m_hash in iota_hashes
    return found

def validate_iota_data(raw_iota_transaction_data):
    """
    Validate a hash from Iota, i.e. Iota entry begins the value of CERATAG.
    """
    txs_data = get_decoded_tx_data(raw_iota_transaction_data)
    txs_data_decrypted = list([decrypt_fernet(FERNETKEY, tx) 
                               for tx in txs_data])
    valid_txs_data = list(filter(lambda x: x[:CERATAGLENGTH] == CERATAG,
                                 txs_data_decrypted))
    return valid_txs_data


def calculate_sha256(s):
    """
    Calculate sha256 hash for a string.
    """
    m = hashlib.sha256()
    m.update(s.encode('utf-8'))
    return m.hexdigest()

def encrypt_fernet(key, s):
    """
    Encrypt a string using Fernet with a given key. Returns a string.
    """
    token = fernet_suite.encrypt(s.encode('utf-8'))
    token_string = token.decode('utf-8')
    return token_string

def decrypt_fernet(key, tokenstring):
    """
    Decrypt a Fernet token (string) with the key. Returns a plaintext string.
    """
    token = tokenstring.encode('utf-8')
    decrypted_bytes = fernet_suite.decrypt(token)
    decrypted_string = decrypted_bytes.decode('utf-8')
    return decrypted_string

def rest_write(job_id, data, fingerprint, xmlstring, dcc):
    """
    Aggregate data to be written to Iota and db, and manage the writing thread.
    """
    try:
        data_to_write = {
                'crane_name': data['crane_name'],
                'load_gross_t': data['load_gross'],
                'load_tared_t': data['load_tared'],
                'date_time': data['datetime'],
                'container_id': data['containerid'],
                'xml_string': xmlstring,
                'fingerprint': fingerprint,
                'dcc': dcc
                }
        store_data(data_to_write)
        threads[job_id]['status'] = 'success'
        threads[job_id]['iscomplete'] = True

    except Exception as e:
        app.logger.info(e)
        threads[job_id]['status'] = 'failed'
        threads[job_id]['iscomplete'] = True
        threads[job_id]['hasfailed'] = True


def store_data(data, verbose=False):
    """
    Form the string to store to Iota and db, and store it to both.

    The string to store is formed in the following way:
    1) Function 'measurement_hash' creates a JSON from the given data and 
       calculates SHA-256 hash for the JSON.
    2) Encrypt the the SHA-256 hash with Fernet.
    
    The resulting ciphertext is then send to Iota with the container's id
    as the tag for the Iota transaction. Once the identifying hash for the Iota
    transaction is available, the same data is stored in the database.

    :param dict data: The measurement data to be stored. Should contain keys
                      crane_name, load_gross_t, load_tared_t, date_time,
                      container_id, fingerprint, xml_string, and dcc.
    """
    tx_data = measurement_hash(data)
    encrypted_tx_data = encrypt_fernet(FERNETKEY, tx_data)

    if verbose:
        print('...WRITING TO IOTA...')

    try:
        data['iota_tx_hash'] = confirmed_write(tag=data['container_id'],
                                               data=encrypted_tx_data)
    except Exception as e:
        app.logger.info(e)
        raise e

    if verbose:
        print('...WRITING TO DB...')
    try:
        db = CeraDB()
        db.insert_weighing_record(crane_name   =   data['crane_name'], 
                                  load_gross_t =   data['load_gross_t'],
                                  load_tared_t =   data['load_tared_t'], 
                                  date_time    =   data['date_time'],
                                  container_id =   data['container_id'], 
                                  fingerprint  =   data['fingerprint'],
                                  xml_string   =   data['xml_string'],
                                  iota_tx_hash =   data['iota_tx_hash'],
                                  dcc          =   data['dcc'])
    except Exception as e:
        app.logger.info(e)
        raise e


app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
threads = {} 


@app.route('/api/xml', methods=['GET'])
@cross_origin()
def get_xml():
    if request.args.get('tx') is None:
        return json.dumps({'message': "Parameter 'tx' not found."})
    tx_hash = request.args.get('tx')
    try:
        db = CeraDB()
        criteria = {"iota_tx_hash": tx_hash}
        query_result = db.fetch_weighing_records(criteria)
        colnames = db.get_colnames()
        response_list = []
        for t in query_result:
            response_dict = tuple_to_dict(t, colnames) 
            #response_dict['iota_confirmed'] = check_integrity(t)
            response_list.append(response_dict)
    except Exception as e:
        app.logger.info(e)
        return json.dumps({'message': "Failed to read from database."})
    xml_str = response_list[0]['xml_string']
    return Response(xml_str, mimetype='text/xml')



@app.route('/api/read', methods=['GET'])
@cross_origin()
def read_db_iota():
    """
    Get a JSON of measurement data. Takes a REST parameter "containerid".

    Get JSON Array of JSON objects with each object representing a row from the
    query result. The attributes of the objects equal the column names of the
    database. For example:

    [{"crane_name": "Ilmatar", "load_gross_t": 3.02,...},
     {"crane_name": "Ilmatar", "load_gross_t": 2.99,..},
     ...
     {"crane_name": "Ilmatar", "load_gross_t": 3.11,..}]
    """
    if request.args.get('containerid') is None:
        return json.dumps({'message': "Parameter 'containerid' not found."})
    container_id = request.args.get('containerid')

    # Get all the fingerprints for the container from Iota.
    txs = read_from_tangle(tx_tags=[container_id, ])
    valid_txs_data = validate_iota_data(txs)

    # Read from the database.
    try:
        db = CeraDB()
        criteria = {"container_id": container_id}
        query_result = db.fetch_weighing_records(criteria)
        colnames = db.get_colnames()
        response_list = []
        # Check if iota and db have the same number of hashes for the given id.
        n_hashes_match = len(valid_txs_data) == len(query_result)
        for t in query_result:
            response_dict = tuple_to_dict(t, colnames)
            if n_hashes_match:
                found_in_iota = check_integrity(response_dict, valid_txs_data)
                response_dict['iota_confirmed'] = found_in_iota
            else:
                response_dict['iota_confirmed'] = False
            response_list.append(response_dict)
    except Exception as e:
        app.logger.info(e)
        raise e
    else:
        return json.dumps(response_list, default=to_string)


@app.route('/api/write', methods=['POST'])
@cross_origin()
def write():
    """
    Write measurement data from a JSON to Iota and db over REST.

    Takes as a REST parameter a JSON with fields:
    * data: Measurement data as a JSON.
    * signature_hash: Hash of the signature of the measurement.
    * xmlstr: Measurement XML as a Base64 encoded string.
    * dccstr: Crane's DCC as a Base 64 encoded (?) string.
    """
    if not request.json:
        abort(400)
    payload = request.get_json(force=True)
    data = payload['data']
    fingerprint = payload['signature_hash']
    xmlstring = payload['xmlstr']
    dcc = payload['dccstr']

    print(validate_container_id(data['containerid']))
    if not validate_container_id(data['containerid']):
        abort(400)

    # Create thread
    job_id = str(uuid.uuid4())
    thread = Thread(target=rest_write, 
                    kwargs={
                        'job_id': job_id, 
                        'data': data, 
                        'fingerprint': fingerprint, 
                        'xmlstring': xmlstring,
                        'dcc': dcc})
    threads[job_id] = {
        'status': 'working',
        'job_id': job_id,
        'iscomplete': False,
        'hasfailed': False
        }
    thread.start()
    return jsonify(threads[job_id]), 201


@app.route('/api/getstatus', methods=['GET'])
@cross_origin()
def get_status():
    """Get the status of a Iota/db writing job. Takes a REST parameter 'id'."""
    if request.args.get('id') is None:                                          
        ret = {'message': "Invalid job id"}                                         
    job_id = str(request.args.get('id'))                                            
    try:
        ret = threads[job_id] 
        return json.dumps(ret) 
    except Exception as e:
        ret = {'message': "Invalid job id"}
        return json.dumps(ret)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port='9010')
