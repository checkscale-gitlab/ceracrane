import unittest
import requests
import random
import random
import string
import json
import psycopg2
from time import sleep
from time import time
from datetime import datetime


address = "http://db-rest:9010/api"

def create_test_data():
    current_time = time()
    epoch_time = str(int(current_time))
    test_sighash = 'SIG' + epoch_time
    test_xml = 'TESTXML' + epoch_time
    container_id = "CERA" + epoch_time
    test_load_gross = random.uniform(3.0, 7.0)
    test_load_tared = random.uniform(1.0, 3.0)
    current_datetime = str(datetime.fromtimestamp(current_time).
            strftime("%Y-%m-%d %H:%M:%S"))
    testdata = { 
            "data": {
                "crane_name": "Ilmatar",
                "load_gross": test_load_gross,
                "load_tared": test_load_tared,
                "datetime": current_datetime,
                "containerid": container_id
                },
            "signature_hash": test_sighash,
            "xmlstr": test_xml,
            "dccstr": "MOCK-DCC-STRING"
            }
    return testdata

def write_test_data(container_id=None):
    testdata = create_test_data()
    if container_id:
        testdata['data']['containerid'] = container_id
    response = requests.post(
            address + "/write",
            json = testdata)
    job_id = response.json()['job_id']
    return (testdata, job_id)

def confirm_write(job_id):
    ready = False
    while not ready:
        response = requests.get(
                address + "/getstatus?id=" + job_id)
        ready = response.json()['iscomplete']
        sleep(1)
    return ready

def write_test_data_confirm(container_id=None):
    (testdata, job_id) = write_test_data(container_id)
    confirm_write(job_id)
    return testdata



class TestDBRest(unittest.TestCase):
    def testBasicReadWrite(self):
        testdata = write_test_data_confirm()
        container_id = testdata['data']['containerid']
        response = requests.get(address + \
                "/read?containerid=" + container_id)
        response_json = response.json()
        self.assertEqual(response_json[0]["crane_name"], "Ilmatar")
        self.assertEqual(response_json[0]["iota_confirmed"], True)

    def testRWEmptyCID(self):
        testdata = create_test_data()
        testdata['data']['containerid'] = ''
        response = requests.post(
                address + "/write",
                json = testdata)
        self.assertEqual(response.status_code, 400)

    def testRWSpacyCID(self):
        testdata = create_test_data()
        testdata['data']['containerid'] = 'C I D'
        response = requests.post(
                address + "/write",
                json = testdata)
        self.assertEqual(response.status_code, 400)

    def testRWUmlautCID(self):
        testdata = create_test_data()
        testdata['data']['containerid'] = 'CÖNTÄINER-ID'
        response = requests.post(
                address + "/write",
                json = testdata)
        self.assertEqual(response.status_code, 400)

    def testRWNotJson(self):
        response = requests.post(
                address + "/write?id=100")
        self.assertEqual(response.status_code, 400)

    def testReadWrongCID(self):
        testdata = write_test_data_confirm()
        container_id = testdata['data']['containerid'][:-1]
        response = requests.get(address + \
                "/read?containerid=" + container_id)
        self.assertEqual(response.text, '[]')

    def testIotaValidationDataChange(self):
        testdata = write_test_data_confirm()
        container_id = testdata['data']['containerid']
        response = requests.get(address + \
                "/read?containerid=" + container_id)
        response_json = response.json()
        self.assertEqual(response_json[0]["iota_confirmed"], True)

        # Alter data in the database.
        conn = psycopg2.connect(host='db',
                                database='postgres',
                                user='postgres',
                                password='example')
        query = """UPDATE measurements 
                   SET load_gross_t = 15.1 
                   WHERE container_id = %s;"""
        cur = conn.cursor()
        cur.execute(query, (container_id, ))
        conn.commit()
        conn.close()

        #Read altered data.
        response = requests.get(address + \
                "/read?containerid=" + container_id)
        response_json = response.json()
        self.assertEqual(response_json[0]["load_gross_t"], 15.1)
        self.assertEqual(response_json[0]["iota_confirmed"], False)

    def testIotaValidationDataDelete(self):
        testdata1 = write_test_data_confirm()
        container_id = testdata1['data']['containerid']
        testdata2 = write_test_data_confirm(container_id)
        response = requests.get(address + \
                "/read?containerid=" + container_id)
        response_json = response.json()
        self.assertEqual(response_json[0]["iota_confirmed"], True)
        self.assertEqual(response_json[1]["iota_confirmed"], True)

        # Alter data in the database.
        conn = psycopg2.connect(host='db',
                                database='postgres',
                                user='postgres',
                                password='example')
        query = """DELETE from measurements 
                   WHERE fingerprint = %s;"""
        cur = conn.cursor()
        cur.execute(query, (testdata1['signature_hash'], ))
        conn.commit()
        conn.close()

        #Read altered data.
        response = requests.get(address + \
                "/read?containerid=" + container_id)
        response_json = response.json()
        self.assertEqual(response_json[0]["fingerprint"],
                testdata2['signature_hash'])
        self.assertEqual(response_json[0]["iota_confirmed"], False)

    def testLargeReadWrite(self):
        n = 10
        jobs = []
        (testdata1, job_id1) = write_test_data()
        sleep(3)
        jobs.append(job_id1)
        container_id = testdata1['data']['containerid']
        for i in range(n-1):
            (testdata, job_id) = write_test_data(container_id)
            jobs.append(job_id)
            sleep(3)
        no_ready = 0
        while no_ready < n:
            for j in jobs:
                response = requests.get(address + "/getstatus?id=" + j)
                no_ready += response.json()['iscomplete']
            sleep(1)

        # Wait to ensure the transactions are readable from the Tangle.
        sleep(15)
        response = requests.get(address + \
                "/read?containerid=" + container_id)
        response_json = response.json()
        self.assertEqual(len(response_json), n)


if __name__ == '__main__':
    unittest.main(verbosity=2)
