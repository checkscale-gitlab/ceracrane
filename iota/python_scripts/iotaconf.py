from iota import Iota

# Key for the Fernet cipher used in db-rest.
FERNET_KEY = b'LM-vmKSBrGX8XYLUp3aw6dnkKktUYEZ3hSi5ddWfbvo='

# DB environment variables
DBHOST = 'db'
DBUSERNAME = 'postgres'
DBPASSWORD = 'example'
DBPORT = '5432'
DB = 'postgres'

# The address of the Iota's devnet Tangle
DEVNET = 'https://nodes.comnet.thetangle.org/'

# Generate addresses using the given seed. The project uses the first address
# generated unless otherwise specified for the constant ADDRESS. The addresses
# are saved in the file iota_addresses.txt.
seed = b'CERASEED'
api = Iota(DEVNET, seed)
gna_result = api.get_new_addresses(count=10)
addresses = gna_result['addresses']

with open('iota_addresses.txt', "w") as f:
    for a in addresses:
        f.write(str(a) + '\n')

ADDRESS_LIST = addresses
ADDRESS = ADDRESS_LIST[3]


#
# Below is the old address used during the project development. The address
# contains a lot of transaction which cause random problems with the Iota API
# and hence should not be used any more. It is stored here in case there is a
# need to check something that is written to it. 
#
# 'JOKLOARHKXQCVPPVVIPIJGLUTLTKFHYGMBBLOXJFYGSARLOTYFFSDZNYCOBOCNPGRMJWZCQBNOROUCE9K'
#

