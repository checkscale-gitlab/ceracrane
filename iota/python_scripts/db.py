import psycopg2
import argparse
import iotaconf

INSERT_QUERY = \
            "INSERT INTO measurements (\
            crane_name, \
            load_gross_t, \
            load_tared_t, \
            date_time, \
            container_id, \
            fingerprint, \
            xml_string, \
            iota_tx_hash, \
            dcc) \
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);"

class CeraDB:
    """Provides a high level interface for the postgres database in the Ceracrane project,
    with supporting project-specific database operations.

    Args:
        user (str): Postgres username
        password (str): Postgres password
        host (str): Postgres host name
        port (str): Postgres port number
        database (str): database name

    Attributes:
        connection (connection): Psycopg2 connection object
        cursor (cursor): Psycopg2 cursor object
    """
    def __init__(self, user=iotaconf.DBUSERNAME, password=iotaconf.DBPASSWORD, 
            host=iotaconf.DBHOST, port=iotaconf.DBPORT, database=iotaconf.DB):
        self.connection = psycopg2.connect(user=user,
                                           password=password,
                                           host=host,
                                           port=port,
                                           database=database)
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.cursor.close()
        self.connection.close()

    def insert_weighing_record(self, crane_name, load_gross_t, load_tared_t,
            date_time, container_id, fingerprint, xml_string, iota_tx_hash,
            dcc):
        """Insert weighing record into database.
        
        Args:
            crane_name:
            load_gross_t:
            load_tared_t:
            date_time:
            container_id:
            fingerprint:
            xml_string:
            iota_tx_hash:
            dcc:
        """
        data = (crane_name, load_gross_t, load_tared_t, date_time,
                container_id, fingerprint, xml_string, iota_tx_hash, dcc)
        self.cursor.execute(INSERT_QUERY, data)
        self.connection.commit()

    def fetch_weighing_records(self, criteria):
        """Fetch weighing records with matching attributes from database
        
        Args:
            criteria (Dict): Dict containing attribute-value pairs to be used as 
            criteria for finding records in table

        Returns:
            List of tuples containing the weighing records with attributes
            matching the criteria
        """
        query = "SELECT * FROM measurements WHERE " + \
                " AND ".join([k + " = %s" for k in criteria.keys()]) + ";"
        self.cursor.execute(query, list(criteria.values()))
        result = self.cursor.fetchall()
        return result
    
    def get_colnames(self):
        return [desc[0] for desc in self.cursor.description]

if __name__ == "__main__":
    criteria = {"container_id": "CERA7428ILM899271-37281"}

    db = CeraDB()
    criteria = {"container_id": "CERA7428ILM899271-37281"}
    result = db.fetch_weighing_records(criteria)
    print(result)
